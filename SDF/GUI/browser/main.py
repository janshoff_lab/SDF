import os
import sys

from PyQt5.QtCore import QDir
from PyQt5.QtCore import Qt
from PyQt5.QtGui import QKeyEvent
from PyQt5.QtWidgets import QApplication, QSplitter
from PyQt5.QtWidgets import QPushButton, QHBoxLayout, QVBoxLayout, QFileDialog, QWidget

from SDF.GUI.browser.preview import DataPreviewWidget
from SDF.GUI.browser.sdf_tree import SDFTreeWidget
from SDF.GUI.browser.summary import SummaryWidget
from SDF.file_io import load_from_sdf


class SDFBrowser(QWidget):
    """
    Widget to browse through any sdf file, displayed in a tree view.
    Also displays some detailed information and image data previews.
    """
    def __init__(self):
        super().__init__()

        # directory to open
        self.open_dir = QDir.currentPath()

        # init UI
        self.summary_widget = SummaryWidget()
        self.data_preview_widget = DataPreviewWidget()
        self.treeview = SDFTreeWidget(parent=self, data_preview_widget=self.data_preview_widget,
                                      summary_widget=self.summary_widget)
        self.init_ui()

    def init_ui(self):
        """
        Initializes all widgets (buttons, labels, etc.) displayed
        on this widget.
        """
        self.setWindowTitle('SDF Browser')

        # splitter (makes left and right part resizable)
        splitter = QSplitter(parent=self)
        splitter_layout = QHBoxLayout()
        splitter_layout.addWidget(splitter)
        splitter_layout.setContentsMargins(0, 0, 0, 0)
        self.setLayout(splitter_layout)

        # left and right panels
        left_panel_widget = QWidget()
        right_panel_widget = QWidget()
        splitter.addWidget(left_panel_widget)
        splitter.addWidget(right_panel_widget)
        left_panel = QVBoxLayout()
        right_panel = QVBoxLayout()
        left_panel_widget.setLayout(left_panel)
        right_panel_widget.setLayout(right_panel)

        # left: SDFObject tree
        left_panel.addWidget(self.treeview)

        # left: Open button
        hbox_open = QHBoxLayout()
        btn_open_sdf = QPushButton("Open SDF files")
        btn_open_sdf.clicked.connect(self.open_files)
        hbox_open.addWidget(btn_open_sdf)
        hbox_open.addStretch(1)
        left_panel.addLayout(hbox_open)

        # right: summary and preview
        right_panel.addWidget(self.summary_widget)
        right_panel.addWidget(self.data_preview_widget)

    def open_files(self) -> None:
        """opens dialog for opening sdf files, adds selected files to the browser"""
        # get file dialog
        files, _ = QFileDialog.getOpenFileNames(self, 'Open SDF file', self.open_dir, '*sdf')

        # read files and add them to widget
        for file in files:
            sdf_obj = load_from_sdf(file)
            self.treeview.add_sdf_object(sdf_obj)

            self.open_dir = os.path.split(file)[0]

    def keyPressEvent(self, event: QKeyEvent) -> None:
        if event.key() == Qt.Key_O and event.modifiers() == Qt.ControlModifier:
            self.open_files()


def main():
    app = QApplication([])
    app.setAttribute(Qt.AA_UseStyleSheetPropagationInWidgetStyles, True)
    if "-d" in sys.argv or "--debug" in sys.argv:
        import logging
        logging.basicConfig(level=logging.DEBUG)
    gui = SDFBrowser()
    gui.show()
    exit(app.exec_())
