"""Global constants"""

# for formatting XML output files
TABSIZE = 3
LINEWIDTH = 120

DEFAULT_DATE_FORMAT = '%Y.%m.%d %H:%M:%S'
ENCODING = "utf-8"
