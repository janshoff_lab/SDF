import logging
from typing import Optional, Iterable
from xml.etree.ElementTree import Element

logger = logging.getLogger(__name__)


def pop_child_element(element: Element, index: int = -1) -> Element:
    """Remove and return the specified child element (mimics `list.pop`)"""
    child = element[index]
    element.remove(child)
    return child


def pop_all_child_elements(element: Element) -> Iterable[Element]:
    """Iterate over all child elements, removing them in the process"""
    while len(element) > 0:
        yield pop_child_element(element, 0)


def pop_element_attribute(element: Element, key: str) -> str:
    """Remove and return specified element attribute value"""
    return element.attrib.pop(key)


def pop_element_text(element: Element) -> str:
    """Remove and return the text node of the element"""
    ret = element.text
    element.text = None
    return ret


def element_is_empty(element: Element) -> bool:
    """Return true if the element has no children, no attributes and no (or whitespace-only) text"""
    if len(element) > 0:
        logger.debug(f"element has children: {list(element)}")
        return False
    if element.attrib:
        logger.debug(f"element has attributes: {element.attrib}")
        return False
    if element.text and not element.text.isspace():  # text can be None, empty string or non-empty string
        print(f"element has text: {element.text!r}")
        return False
    return True


def unindent_xml_text(text: str) -> str:
    """Unindent given text to its minimum indentation level. Relative indentation is kept."""
    def is_empty(s: str):
        return not s or s.isspace()

    if text.isspace():
        return ""

    shortest_indent: Optional[str] = None
    # get indent of shortest non-empty line
    for line_num, line in enumerate(text.splitlines(keepends=False)):
        if is_empty(line):
            continue

        indent = ""
        for i, char in enumerate(line):
            if not char.isspace():
                indent = line[:i]
                break
        if shortest_indent is None or len(indent) < len(shortest_indent):
            shortest_indent = indent

    return "\n".join(line[len(shortest_indent):].rstrip()
                     for line in text.splitlines(keepends=False)
                     if not is_empty(line))
