from xml.etree.ElementTree import Element

from SDF.data_model._helper_functions import pop_element_text, element_is_empty
from SDF.data_model.abstract import XMLWritable


class Name:
    """Represents an SDF <name> element"""
    __name: str
    name: str

    def __init__(self, name: str):
        if not isinstance(name, str):
            raise TypeError(f"Expected a string, got {type(name)}")
        if "\n" in name:
            raise ValueError("Name must be a single line")
        if not name or name.isspace():
            raise ValueError("Name cannot be empty")
        if name[0].isspace() or name[-1].isspace():
            raise ValueError("Name cannot start or end with whitespace")
        self.__name = name

    @property
    def name(self):
        return self.__name

    def __repr__(self):
        return f"{self.__class__.__name__}({self.name!r})"

    def __eq__(self, other):
        if isinstance(other, Name):
            return self.name == other.name
        return False


class NameElement(Name, XMLWritable):
    def __init__(self, name: str):
        super().__init__(name)

    def to_xml_element(self) -> Element:
        element = Element("name")
        element.text = self.name
        return element

    @classmethod
    def from_xml_element(cls, element: Element) -> "NameElement":
        if element.tag != "name":
            raise ValueError(f"Expected a <name> element, got {element.tag}")

        text = pop_element_text(element)

        if not element_is_empty(element):
            raise ValueError("Element is not empty")

        return cls(text.strip())
