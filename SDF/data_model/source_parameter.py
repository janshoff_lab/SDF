import getpass
import os
import platform

from SDF import __version__
from SDF.data_model.parameter import ParameterSet, Parameter


class SourceParameters(ParameterSet):
    """Represents an <par name="source-of-sdf-file"> element. Carries information about the origin of the SDF file."""
    def __init__(self, converter_name: str = None, source_file: str = None, source_file_type: str = None):
        super().__init__(name="source-of-sdf-file")

        # system-specific parameters
        self.add(Parameter("user-name", getpass.getuser()))
        self.add(Parameter("computer-name", platform.uname().node))
        self.add(Parameter("sdf-version", __version__))

        # conversion-specific parameters
        if converter_name is not None:
            if not isinstance(converter_name, str):
                raise TypeError(f"Expected a string, got {type(converter_name)}")
            self.add(Parameter("converter", converter_name))
        if source_file is not None:
            if not isinstance(source_file, str):
                raise TypeError(f"Expected a string, got {type(source_file)}")
            if os.path.isfile(source_file):
                source_file = os.path.abspath(source_file)
            self.add(Parameter("source-file", source_file))
        if source_file_type is not None:
            if not isinstance(source_file_type, str):
                raise TypeError(f"Expected a string, got {type(source_file_type)}")
            self.add(Parameter("source-file-type", source_file_type))
