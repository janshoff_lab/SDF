from xml.etree.ElementTree import Element

from SDF.data_model.abstract import XMLWritable
from SDF.data_model.sdf_object import SDFObject


class Dataset(SDFObject, XMLWritable):
    """Abstract class. Dataset implementations must inherit from it."""
    @staticmethod
    def from_xml_element(element: Element) -> "Dataset":
        """Reads the 'type' attribute of a <dataset type=...> element, delegates to the associated Dataset subclass"""
        if element.tag != "dataset":
            raise ValueError(f"Expected a dataset element, got {element.tag}")

        ds_type = element.attrib["type"]
        if ds_type == "sc":
            from SDF.data_model.array_dataset_1d import ArrayDataset1D
            return ArrayDataset1D.from_xml_element(element)
        if ds_type == "mc":
            from SDF.data_model.array_dataset_2d import ArrayDataset2D
            return ArrayDataset2D.from_xml_element(element)
        if ds_type == "img":
            from SDF.data_model.image_dataset import ImageDataset
            return ImageDataset.from_xml_element(element)
        else:
            raise ValueError(f"Unknown dataset type: {ds_type}")
