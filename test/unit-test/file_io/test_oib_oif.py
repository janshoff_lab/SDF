from io import StringIO
import unittest

from SDF.data_model import ImageDataset
from SDF.file_io import load_from_oib, load_from_sdf, write_sdf


class TestOIF(unittest.TestCase):
    def test_oib_with_int_index(self):
        sdf = load_from_oib("example-input/oib-int_index.oib")

        self.assertTrue("FV1000" in sdf.instruments)
        self.assertIsInstance(sdf["channel 1"]["image 1"], ImageDataset)
        self.assertEqual(sdf["channel 1"]["image 1"].data.size, (800, 800))

        sdf_file = StringIO()
        write_sdf(sdf, sdf_file)
        sdf_file.seek(0)
        self.assertEqual(sdf, load_from_sdf(sdf_file))

    def test_oib_with_plane_index(self):
        sdf = load_from_oib("example-input/oib-plane_index.oib")

        self.assertTrue("FV1000" in sdf.instruments)
        self.assertIsInstance(sdf["channel 1"]["image 1"], ImageDataset)
        self.assertEqual(sdf["channel 1"]["image 1"].data.size, (640, 640))

        sdf_file = StringIO()
        write_sdf(sdf, sdf_file)
        sdf_file.seek(0)
        self.assertEqual(sdf, load_from_sdf(sdf_file))

    def test_oib_with_preview_and_time_index(self):
        sdf = load_from_oib("example-input/oif-with_previews-time_index.oif")

        self.assertTrue("FV1000" in sdf.instruments)
        self.assertIsInstance(sdf["channel 1"]["image 1"], ImageDataset)
        self.assertEqual(sdf["channel 1"]["image 1"].data.size, (512, 512))

        sdf_file = StringIO()
        write_sdf(sdf, sdf_file)
        sdf_file.seek(0)
        self.assertEqual(sdf, load_from_sdf(sdf_file))
