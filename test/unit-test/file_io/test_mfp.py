import logging
import unittest
from io import StringIO, BytesIO

import numpy as np
from scipy.io import loadmat

from SDF.file_io import write_sdf, load_from_sdf, load_from_mfp, sdf_to_force_sdf, force_sdf_to_mat


class TestMFP(unittest.TestCase):
    def test_force(self):
        sdf = load_from_mfp("example-input/ibw-curve.ibw")
        self.assertEqual(len(sdf.workspaces), 1)
        self.assertEqual(sdf["Data"]["Raw"].data.shape, (13_296,))

        fsdf = sdf_to_force_sdf(sdf)
        self.assertEqual(fsdf.instruments["parameters"]["sensitivity"].unit, "m/V")
        self.assertEqual(fsdf.instruments["parameters"]["spring_constant"].unit, "N/m")
        self.assertEqual(fsdf["segment 0"]["height"].unit, "m")
        self.assertEqual(fsdf["segment 0"]["vDeflection"].unit, "m")
        self.assertEqual(fsdf["segment 0"]["measuredHeight"].unit, "m")
        self.assertEqual(fsdf["segment 0"]["measuredTipSample"].unit, "m")
        self.assertEqual(fsdf["segment 0"]["tipSample"].unit, "m")

        mat_file = BytesIO()
        force_sdf_to_mat(fsdf, mat_file)
        mat = loadmat(mat_file, simplify_cells=True)  # simplity_cells squeezes unit dimensions and more
        self.assertTrue(np.allclose(mat["segment_0"]["height"], fsdf["segment 0"]["height"].data))
        self.assertEqual(mat["segment_0"]["height_unit"], fsdf["segment 0"]["height"].unit)
        self.assertTrue(np.isclose(float(mat["spring_constant"]),
                                   fsdf.instruments["parameters"]["spring_constant"].parsed_value))
        self.assertEqual(mat["spring_constant_unit"], fsdf.instruments["parameters"]["spring_constant"].unit)

        sdf_file = StringIO()
        write_sdf(sdf, sdf_file)
        sdf_file.seek(0)
        self.assertEqual(sdf, load_from_sdf(sdf_file))

        fsdf_file = StringIO()
        write_sdf(fsdf, fsdf_file)
        fsdf_file.seek(0)
        self.assertEqual(fsdf, load_from_sdf(fsdf_file))

    def test_force_no_conversions(self):
        sdf = load_from_mfp("example-input/ibw-curve-no-conversions.ibw")

        with self.assertLogs(logging.getLogger(), logging.WARNING):
            # sensitivity and spring_constant are fallback values
            fsdf = sdf_to_force_sdf(sdf)

        self.assertIsNone(fsdf.instruments["parameters"]["sensitivity"].unit)
        self.assertIsNone(fsdf.instruments["parameters"]["spring_constant"].unit)
        self.assertEqual(fsdf["segment 0"]["height"].unit, "m")
        self.assertEqual(fsdf["segment 0"]["vDeflection"].unit, "m")
        self.assertEqual(fsdf["segment 0"]["measuredHeight"].unit, "m")

        mat_file = BytesIO()
        force_sdf_to_mat(fsdf, mat_file)
        mat = loadmat(mat_file, simplify_cells=True)  # simplity_cells squeezes unit dimensions and more
        self.assertTrue(np.allclose(mat["segment_0"]["height"], fsdf["segment 0"]["height"].data))
        self.assertEqual(mat["spring_constant_unit"], "None")

        sdf_file = StringIO()
        write_sdf(sdf, sdf_file)
        sdf_file.seek(0)
        self.assertEqual(sdf, load_from_sdf(sdf_file))

        fsdf_file = StringIO()
        write_sdf(fsdf, fsdf_file)
        fsdf_file.seek(0)
        self.assertEqual(fsdf, load_from_sdf(fsdf_file))

    def test_map(self):
        sdf = load_from_mfp("example-input/ibw-map.ibw")

        self.assertEqual(sdf["Data"]["MapHeight"].data.shape, (10, 10))

        sdf_file = StringIO()
        write_sdf(sdf, sdf_file)
        sdf_file.seek(0)
        self.assertEqual(sdf, load_from_sdf(sdf_file))
