from io import StringIO
import unittest

from SDF.data_model import ImageDataset
from SDF.file_io import load_from_lsm, load_from_sdf, write_sdf


class TestLSM(unittest.TestCase):
    def test_lsm(self):
        sdf = load_from_lsm("example-input/lsm-test.lsm")

        self.assertTrue("time-stamps" in sdf.datasets)
        self.assertTrue("channel colors" in sdf.datasets)
        self.assertEqual(sdf["channel colors"].data.shape, (52, 2))
        self.assertIsInstance(sdf["series_0"]["layer_0"]["img_0"], ImageDataset)
        self.assertEqual(sdf["series_0"]["layer_0"]["img_0"].data.size, (1024, 1024))

        sdf_file = StringIO()
        write_sdf(sdf, sdf_file)
        sdf_file.seek(0)
        self.assertEqual(sdf, load_from_sdf(sdf_file))
