import unittest
from xml.etree import ElementTree

from SDF.data_model.name import Name, NameElement


class TestName(unittest.TestCase):
    @classmethod
    def setUpClass(cls) -> None:
        cls.valid_element_abc = ElementTree.fromstring(
            """
    <name>
        abc
    </name>
            """
        )
        cls.valid_element_abc_def = ElementTree.fromstring(
            """
    <name>
        abc  def
    </name>
            """
        )
        cls.invalid_element_newline = ElementTree.fromstring(
            """
    <name>
        abc
        def
    </name>
            """
        )
        cls.invalid_element_tag = ElementTree.fromstring(
            """
    <nam>
        abc
    </nam>
            """
        )
        cls.invalid_element_attrib = ElementTree.fromstring(
            """
    <name attr="def">
        abc
    </name>
            """
        )

    def test_init(self):
        # invalid arguments
        with self.assertRaises(TypeError, msg="name is not optional"):
            Name()
        with self.assertRaises(ValueError, msg="name cannot be empty"):
            Name("")
        with self.assertRaises(ValueError, msg="whitespace-only name is empty after stripping whitespace"):
            Name(" ")
        with self.assertRaises(ValueError, msg="name must be single line"):
            Name("a\nb")
        with self.assertRaises(TypeError, msg="name must be str"):
            Name(b"abc")
        with self.assertRaises(TypeError, msg="name must be str"):
            Name(["a", "b", "c"])
        with self.assertRaises(TypeError, msg="name must be str"):
            Name(1)
        with self.assertRaises(ValueError, msg="name must not start with whitespace"):
            Name(" abc")
        with self.assertRaises(ValueError, msg="name must not end with whitespace"):
            Name("abc ")

        # name sanitization
        self.assertEqual(Name("abc").name, "abc", msg="simple case")
        self.assertEqual(Name("abc def").name, "abc def", msg="single space")
        self.assertEqual(Name("abc  def").name, "abc  def", msg="multiple spaces")

    def test_property_name(self):
        name = Name("abc")
        with self.assertRaises(AttributeError, msg="name must be read-only"):
            name.name = "def"

    def test_to_xml_element(self):
        element = NameElement("abc").to_xml_element()
        self.assertEqual(len(element.attrib), 0, msg="<name> elements have no attributes")
        self.assertEqual(element.text, "abc", msg="no leading and trailing whitespace "
                                                  "(leave formatting to the XML writer)")
        self.assertEqual(element.tag, "name", msg="<name> tag")
        self.assertEqual(len(element), 0, msg="element must have no children")

    def test_from_xml_element(self):
        self.assertEqual(NameElement.from_xml_element(self.valid_element_abc), Name("abc"), msg="simple case")
        self.assertEqual(NameElement.from_xml_element(self.valid_element_abc_def), Name("abc  def"), "with whitespace")
        with self.assertRaises(ValueError, msg="wrong tag"):
            NameElement.from_xml_element(self.invalid_element_tag)
        with self.assertRaises(ValueError, msg="newline in name"):
            NameElement.from_xml_element(self.invalid_element_newline)
        with self.assertRaises(ValueError, msg="attributes"):
            NameElement.from_xml_element(self.invalid_element_attrib)

    def test_eq(self):
        self.assertNotEqual(Name("abc"), Name("abcdef"))
        self.assertEqual(Name("abc"), Name("abc"))
        self.assertEqual(Name("abc def"), Name("abc def"))
        self.assertEqual(Name("abc  def"), Name("abc  def"))
        self.assertNotEqual(Name("abc"), "abc")

    def test_repr(self):
        self.assertEqual(eval(Name("abc").__repr__()), Name("abc"))
