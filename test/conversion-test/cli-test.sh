#!/bin/sh
# just check if the converter does not raise exceptions
# convert all example files, ignore warnings

set -e  # fail on error

sdf-convert example-input/* --mat -s -o example-output
rm -r example-output
