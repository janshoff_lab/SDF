Getting SDF: Installation and Updates
*************************************

Requirements: Python >= 3.6 and the pip package manager

The SDF project is developed in a `public gitlab repository <https://gitlab.gwdg.de/sdf-project/SDF>`_.
To install it, first download the `source code as zip file
<https://gitlab.gwdg.de/sdf-project/SDF/-/archive/stable/SDF-stable.zip>`_ and then use pip
to install it:

.. code::

   $ pip install [file].zip

To update the current SDF installation, you can either reinstall it as explained above, or use the tool
:code:`sdf-update`.
