Documentation
*************

The SDF documentation is built using `Sphinx <https://www.sphinx-doc.org/en/master/index.html>`_. This page is not a
Sphinx tutorial. To get familiar with Sphinx, consult the documentation of Sphinx itself, or some other tutorial.
This page only addresses some Sphinx extensions and tools used to generate the SDF documentation.

To build the documentation locally, use the Makefile in the ``docs`` directory.

Hosting
=======

The documentation is currently hosted using `GitLab Pages <https://docs.gitlab.com/ee/user/project/pages/>`_.
The CI Pipeline of the SDF source repository runs the tests, an on completion, it triggers a downstream job in the
Pages repository.

sphinx.ext.doctest
==================

The `doctest extension <https://www.sphinx-doc.org/en/master/usage/extensions/doctest.html>`_ integrates the
`doctest <https://docs.python.org/3/library/doctest.html>`_ tool from the Python standard library. Running the doctest
builder (``sphinx-build -b doctest ...``) searches for ``doctest`` directives in the documentation and executes them
as test cases.

.. code-block:: rst

    .. doctest::

        >>> print("Hello World!")
        Hello World!

sphinx-apidoc
=============

The `sphinx-apidoc tool <https://www.sphinx-doc.org/en/master/man/sphinx-apidoc.html>`_ automatically generates sphinx
sources for the ``sphinx.ext.autodoc`` extension.

Running ``sphinx-apidoc -o _apidoc ../SDF ../SDF/extern`` generates sphinx sources for the SDF module (in the ``../SDF``
directory) , excluding the SDF.extern submodule, in the ``_apidoc`` directory.

sphinx.ext.intersphinx
======================

While directives like ``:py:func`` allow references to internally documented Python objects, the
`intersphinx extension <https://www.sphinx-doc.org/en/master/usage/extensions/intersphinx.html>`_ enables us to refer to
other modules like the Python standard library or Numpy.
