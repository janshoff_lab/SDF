GitLab CI
*********

The `GitLab Continuous Integration (CI) <https://docs.gitlab.com/ee/ci/>`_ system can be used to automate tasks whenever
something in a repository changes. The CI pipelines are defined in a top-level file ``.gitlab-ci.yml``

Test Automation
===============

Currently, the SDF repository only uses CI for test automation, using something like this:

.. code-block:: yaml
    :caption: .gitlab-ci.yml

    image: python:3.6-buster

    before_script:
      - export DEBIAN_FRONTEND="noninteractive"  # else apt asks for stuff
      - apt update
      - apt install -y libglib2.0-0 libgl1  # pyqt5 requirements
      - pip3 install -U .
      - pip3 install -r docs/doc-requirements.txt

    test:
      stage: build
      script:
      - python3 -m unittest
      - make --directory docs test

The ``image`` defines a Docker image used for running the pipeline. The ``before_script`` installs SDF and the
documentation requirements. The ``test`` pipeline runs the ``script`` at the ``build`` stage of the pipeline.

Documentation Hosting
=====================

Additionally, the documentation can be hosted via `GitLab Pages <https://docs.gitlab.com/ee/user/project/pages/>`_.
Pages can be used to host any static website.

For that, the ``pages`` pipeline has to be defined such that it produces a static website root
(e.g. containing the file ``index.html``) in the special directory ``public``, which has to be defined as an
``artifact``.

We can use the ``script`` to build the HTML documentation and move it to the ``public`` directory.

.. code-block:: yaml
    :caption: .gitlab-ci.yml

    image: python:3.6-buster

    pages:
      stage: deploy
      script:
      - export DEBIAN_FRONTEND="noninteractive"  # else apt asks for stuff
      - apt update
      - apt install -y libglib2.0-0 libgl1  # pyqt5 requirements
      - wget https://gitlab.gwdg.de/sdf-project/SDF/-/archive/master/SDF-master.tar.gz
      - tar -xzf SDF-master.tar.gz
      - pip3 install ./SDF-master
      - pip3 install -r ./SDF-master/docs/doc-requirements.txt
      - make --directory ./SDF-master/docs html
      - mv ./SDF-master/docs/_build/html ./public
      artifacts:
        paths:
        - public

This can, and currently is, triggered automatically on each push to the source repository.
