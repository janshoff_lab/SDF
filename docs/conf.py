# Configuration file for the Sphinx documentation builder

import os
import sys
import SDF

# -- Project information
project = "SDF"
author = "Burkhard Geil, Filip Savic, Ilyas Kuhlemann, Niklas Mertsch"
copyright = "2021, " + author
release = SDF.__version__

# -- Build settings (http://www.sphinx-doc.org/en/master/config)
exclude_patterns = ['_build', 'img']  # patterns to exclude (also affects html_static_path and html_extra_path)
numfig = True  # number figures, tables and code-blocks (required for referencing with :numref: role)
pygments_style = "sphinx"  # syntax highlighting theme
add_function_parentheses = False  # does not add '()' after function references
add_module_names = False  # don't prepend module names to all object references

# -- HTML output settings
html_theme = 'nature'
html_static_path = ['_static']
html_css_files = ["css/" + file for file in os.listdir("./_static/css")]
html_js_files = ["js/" + file for file in os.listdir("./_static/js")]

# -- Extensions and settings
extensions = []

# built-in extensions
extensions.append("sphinx.ext.doctest")  # parse and test doctest directives
extensions.append("sphinx.ext.autodoc")  # auto-generate documentation for Python objects
extensions.append("sphinx.ext.viewcode")  # view Python source code from documentation

extensions.append("sphinx.ext.todo")  # todo comments
todo_include_todos = True  # show TODO comments in final docs

extensions.append("sphinx.ext.intersphinx")  # refer to external documentation
intersphinx_mapping = {  # sources for external documentation
    'python': ('https://docs.python.org/3', None),
    'numpy': ('https://numpy.org/doc/stable', None),
    'pillow': ('https://pillow.readthedocs.io/en/stable/', None),
    'PyQt5': ('https://www.riverbankcomputing.com/static/Docs/PyQt5/', None),
}

# third-party extensions
extensions.append("sphinx_qt_documentation")  # required for intersphinx with PyQt5 (domain translation for C++ mapping)

extensions.append("sphinxcontrib.apidoc")  # use sphinx-apidoc to auto-generate autodoc directives
apidoc_module_dir = '../SDF'
apidoc_output_dir = '_apidoc'
apidoc_excluded_paths = ['../SDF/extern']
apidoc_separate_modules = True
apidoc_toc_file = False
apidoc_module_first = True
apidoc_extra_args = ['--implicit-namespaces']

extensions.append("sphinxcontrib.runcmd")  # auto-generate command output (e.g. for man pages for --help calls)

# custom extensions
sys.path.append("ext")  # custom extension path

extensions.append("inheritance_diagram")  # generate inheritance diagrams
inheritance_graph_attrs = {  # graphviz attributes
    "rankdir": "LR",  # top to bottom
    "splines": "false"  # only straight edges, no curved lines
}

extensions.append("intersphinx_typing_fix")  # required for intersphinx with typing (domain translation in objects.inv)
