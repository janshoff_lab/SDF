> :warning: **This is the old SDF repository. It will not be updated any longer.**: Find the new repository [here](https://gitlab.gwdg.de/sdf-project/SDF) :warning:

# SDF - Standard Data Format
Find the full documentation on [https://sdf-project.pages.gwdg.de](https://sdf-project.pages.gwdg.de).

## Requirements
Python >= 3.6 and the pip package manager
- On Debian/Ubuntu-based systems, this requires `sudo apt install python3 python3-pip`

Linux, Windows and macOS are supported.

## Getting SDF
### Installation
- One-liner
    - `pip3 install https://gitlab.gwdg.de/sdf-project/SDF/-/archive/stable/SDF-stable.zip`
- step by step
    1. Download the source code from GitLab (`.zip` file)
    2. Open the command line
    3. Run `pip install [...].zip`
        - on some systems, use `pip3` instead

### Updates
Use `sdf-update` to update. This will install the latest stable version 
(equivalent to downloading and pip-installing the .zip file). 
To get the latest, possibly unstable version, use `sdf-update --branch master`.

## Using SDF
### Graphical User Interfaces (GUIs)
- Use `sdf-convert-gui` for converting your files to `.sdf`
- Use `sdf-browser` to inspect `.sdf` files

### Command Line Interface (CLI)
Use `sdf-convert [FILES]`. See `sdf-convert --help` for details. Example usage:

```
sdf-convert file1.lsm file2.oib
    converts file1.lsm and file2.oib to file1.sdf and file2.sdf
    results will be placed alongside the original files

sdf-convert *.lsm
    converts all .lsm files in the current directory to .sdf

sdf-convert *.jpk-force -o sdf
    converts all .jpk-force files to .sdf and .force.sdf
    results will be placed in directory ./sdf

sdf-convert *.jpk-force --no-force --mat
    converts all .jpk files to .sdf and .mat

sdf-convert *
    converts all files in the current directory to .sdf (and .force-sdf, if possible)

sdf-convert --input-format OIB file-without-extension
    reads file-without-extension as .oib file and converts it to file-without-extension.sdf
```

## Python API Usage Examples
### Read and write files
To load a file as SDF, use the appropriate `load_from_[...]` function from the `SDF.file_io` submodule.
To save SDF objects, use the function `write_sdf`.
```python
from SDF.file_io import load_from_lsm, write_sdf

sdf = load_from_lsm("path/to/my-lsm-file.lsm")
write_sdf(sdf, "path/to/my-sdf-file.sdf")
``` 

### Add Parameters to SDFs 
Parameter sets behave mostly like `dict`s. Single parameters can be added from tuples.
```python
from SDF.file_io import load_from_sdf, write_sdf

# load from file
sdf = load_from_sdf("experiment1.sdf")
sdf.parameters["fit-results"] = [
   ("formula", "y = a*x^2 + b"),  # name, value
   ("optimizer", "scipy.optimize.least_squares"),
   ("mean-squared-error", 0.42),
   ("result-params", (0.1, 2.3), "N/s")  # name, value, unit
]

# save sdf object
write_sdf(sdf, "experiment1-with-fit-results.sdf")
```

The result will look like this:
```
   [...]
   <par name="fit-results">
      <par name="formula" value="y = a*x^2 + b"/>
      <par name="optimizer" value="scipy.optimize.least_squares"/>
      <par name="mean-squared-error" value="0.42"/>
      <par name="result-params" value="(0.1, 2.3)" unit="N/s"/>
   </par>
   [...]
```
